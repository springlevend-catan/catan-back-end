FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD /build/libs/catan-0.0.1.jar app.jar
RUN adduser -D myuser
USER myuser
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
CMD java $JAVA_OPTS -jar app.jar -Dserver.port=$PORT $JAR_OPTS