package be.kdg.catan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.context.SecurityContextHolder;

@SpringBootApplication
public class CatanApplication {
    public static void main(String[] args) {
        SpringApplication.run(CatanApplication.class, args);
    }
}
