package be.kdg.catan.email.emailSender;

import be.kdg.catan.shared.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

//Class used to send emails, using the mailSender from the EmailConfig
@Component
public class EmailSend {

    private MailSender mailSender;

    @Value("${authentication.url}")
    private String authenticationUrl;

    @Autowired
    public EmailSend(MailSender mailSender) {
        this.mailSender = mailSender;
    }


    public void sendRegistrationConfirmationMail(User user, String token){
        String recipient = user.getEmail();
        String subject = "Registration Confirmation";
        String url = authenticationUrl + "/account/confirmRegistration?token=" + token;
        String message = "Email confirmation";

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipient);
        email.setSubject(subject);
        email.setText(message + "\n" + url);
        mailSender.send(email);
    }

}
