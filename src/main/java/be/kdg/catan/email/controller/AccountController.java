package be.kdg.catan.email.controller;


import be.kdg.catan.email.exception.TokenNotFoundExcpetion;
import be.kdg.catan.shared.service.TokenService;
import com.auth0.jwt.exceptions.TokenExpiredException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

@Controller
@RequestMapping("/account")
public class AccountController {

    private TokenService tokenService;

    @Autowired
    public AccountController(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    //Mapping for the registration confirmation
    @GetMapping("/confirmRegistration")
    public ResponseEntity<?> confirmRegistration(WebRequest request, Model model, @RequestParam("token") String token) {
        System.out.println("Token request: " + token);
        try {
            tokenService.handleRegisterToken(token);
        } catch (TokenNotFoundExcpetion | TokenExpiredException tokenExcpetion) {
            //return not found code when token is not found
            return new ResponseEntity<>(tokenExcpetion.getMessage(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>("Account activated", HttpStatus.ACCEPTED);
    }
}
