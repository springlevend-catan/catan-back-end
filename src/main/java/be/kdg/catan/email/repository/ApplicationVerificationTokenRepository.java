package be.kdg.catan.email.repository;

import be.kdg.catan.shared.model.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationVerificationTokenRepository extends JpaRepository<VerificationToken, Long>{
    VerificationToken findByToken(String token);
}
