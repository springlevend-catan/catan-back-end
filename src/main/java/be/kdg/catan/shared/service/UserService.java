package be.kdg.catan.shared.service;

import be.kdg.catan.email.emailSender.EmailSend;
import be.kdg.catan.jwt.configuration.JWTConvertor;
import be.kdg.catan.jwt.exception.EmailAlreadyInUseException;
import be.kdg.catan.jwt.exception.EmailNotConfirmedException;
import be.kdg.catan.jwt.exception.UsernameAlreadyInUseException;
import be.kdg.catan.shared.model.User;
import be.kdg.catan.shared.repository.ApplicationUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UserService {
    private ApplicationUserRepository userRepo;
    private AuthenticationManager authenticationManager;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private EmailSend emailSend;
    private TokenService tokenService;

    @Autowired
    public UserService(AuthenticationManager authenticationManager, BCryptPasswordEncoder bCryptPasswordEncoder, ApplicationUserRepository applicationUserRepository, EmailSend emailSend, TokenService tokenService) {
        this.authenticationManager = authenticationManager;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userRepo = applicationUserRepository;
        this.emailSend = emailSend;
        this.tokenService = tokenService;
    }

    /**
     * Check if user exists; if not, add
     *
     * @param user - user to register in the system
     * @throws UsernameAlreadyInUseException - throws this exception if the username already exists.
     * @throws EmailAlreadyInUseException    - throws this exception if the email already exists.
     */
    public void registerUser(User user) throws UsernameAlreadyInUseException, EmailAlreadyInUseException {
        User checkForDuplicateUsername = this.userRepo.findByUsername(user.getUsername());
        User checkForDuplicateEmail = this.userRepo.findByEmail(user.getEmail());

        if (checkForDuplicateUsername != null) {
            throw new UsernameAlreadyInUseException("Username already in use");
        }

        if (checkForDuplicateEmail != null) {
            throw new EmailAlreadyInUseException("Email already in use");
        }

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setEnabled(false);

        this.userRepo.save(user);
        String token = tokenService.generateNewToken(user);

        emailSend.sendRegistrationConfirmationMail(user, token);
    }

    public void checkUser(User user) throws BadCredentialsException, EmailNotConfirmedException {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        user.getUsername(),
                        user.getPassword()
                )
        );

        User u = userRepo.findByUsername(user.getUsername());
        if (!u.isEnabled()) {
            throw new EmailNotConfirmedException("Email not confirmed");
        }
    }

    public List<User> getUsers() {
        return this.userRepo.findAll();
    }

    public void saveUser(User user) {
        userRepo.save(user);
    }

    /**
     * This method will check if users exist with these usernames, otherwise it will return an empty list
     *
     * @param username the part of the username
     * @return an arraylist with the users
     */
    public List<User> findUsersByUsernameContaining(String username) {
        Optional<List<User>> optional = userRepo.findUsersByUsernameContaining(username);
        return optional.orElseGet(ArrayList::new);
    }

    public void deleteUser(User user) {
        userRepo.delete(user);
    }

    public User getUser(String username) {
        return userRepo.findByUsername(username);
    }

    public User getUserByToken(String token) {
        String username = JWTConvertor.getUserNameFromToken(token);
        return userRepo.findByUsername(username);
    }
}
