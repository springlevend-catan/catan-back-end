package be.kdg.catan.shared.service;

import be.kdg.catan.email.exception.TokenNotFoundExcpetion;
import be.kdg.catan.shared.model.User;
import be.kdg.catan.shared.model.VerificationToken;
import be.kdg.catan.email.repository.ApplicationVerificationTokenRepository;
import com.auth0.jwt.exceptions.TokenExpiredException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.UUID;

@Component
public class TokenService {
    private ApplicationVerificationTokenRepository tokenRepository;
    private UserService userService;

    @Autowired
    public TokenService(ApplicationVerificationTokenRepository repository, @Lazy UserService userService) {
        this.tokenRepository = repository;
        this.userService = userService;
    }

    public String generateNewToken(User user) {
        String token = UUID.randomUUID().toString();
        VerificationToken newUserToken = new VerificationToken(token, user);
        tokenRepository.save(newUserToken);
        return token;
    }

    public void handleRegisterToken(String token) throws TokenNotFoundExcpetion, TokenExpiredException {
        VerificationToken verificationToken = tokenRepository.findByToken(token);
        if (verificationToken == null) {
            throw new TokenNotFoundExcpetion("Something went wrong with your token");
        } else {
            User user = verificationToken.getUser();
            Calendar calendar = Calendar.getInstance();
            if ((verificationToken.getExpiryDate().getTime() - calendar.getTime().getTime()) <= 0) {
                throw new TokenExpiredException("Token has expired");
            }
            user.setEnabled(true);
            userService.saveUser(user);
        }
    }
}
