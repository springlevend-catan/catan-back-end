package be.kdg.catan.shared.controller;

import be.kdg.catan.jwt.model.UserDTO;
import be.kdg.catan.shared.model.User;
import be.kdg.catan.shared.repository.ApplicationUserRepository;
import be.kdg.catan.shared.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.rmi.UnexpectedException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class NotificationController {
    private UserService userService;
    private ApplicationUserRepository userRepository;
    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public NotificationController(UserService userService, ApplicationUserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @CrossOrigin(origins = "http://localhost:5500")
    @PostMapping("/user/{userUsername}/has-friend")
    public ResponseEntity<Boolean> hasFriend(@PathVariable String userUsername, @RequestBody String friendUsername) throws UnexpectedException {
        User user = userRepository.findByUsername(userUsername);
        User friend = userRepository.findByUsername(friendUsername);

        // If user doesn't exist, return conflict
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        // If friend doesn't exist, return conflict
        if (friend == null) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        // If user friends already contains friend, return conflict
        if (user.getFriends().contains(friend)) {
            return new ResponseEntity<>(true, HttpStatus.OK);
        }

        return new ResponseEntity<>(false, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/user/{token}/friends")
    public ResponseEntity<List<UserDTO>> getFriends(@PathVariable String token) throws UnexpectedException {
        User user = userService.getUserByToken(token);

        if (user == null) {
            throw new UnexpectedException("User doesn't exist");
        }

        List<UserDTO> userDtos = user.getFriends()
                .stream()
                .map(u -> new UserDTO(u.getUsername(), u.getEmail()))
                .collect(Collectors.toList());

        return new ResponseEntity<>(userDtos, HttpStatus.OK);
    }

    @PostMapping("/user/{token}/add-friend")
    public ResponseEntity<String> addFriend(@PathVariable String token, @RequestBody String friendUsername) throws UnexpectedException {
        User user = userService.getUserByToken(token);

        if (user == null) {
            throw new UnexpectedException("User doesn't exist");
        }

        User friend = userService.getUser(friendUsername);

        if (friend == null) {
            throw new UnexpectedException("User doesn't exist");
        }

        user.getFriends().add(friend);
        friend.getFriends().add(user);

        userService.saveUser(user);
        userService.saveUser(friend);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
