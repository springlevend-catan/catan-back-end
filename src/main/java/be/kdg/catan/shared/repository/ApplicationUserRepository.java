package be.kdg.catan.shared.repository;

import be.kdg.catan.shared.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ApplicationUserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findByEmail(String email);
    Optional<List<User>> findUsersByUsernameContaining(String username);
}
