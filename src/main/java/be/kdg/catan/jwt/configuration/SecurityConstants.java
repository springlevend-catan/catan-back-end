package be.kdg.catan.jwt.configuration;

import org.springframework.beans.factory.annotation.Value;

/**
 * Constants used for security
 */
public class SecurityConstants {
    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users/sign-up";
    public static final String SIGN_IN_URL = "/users/sign-in";
    public static final String GET_USER = "/users/get";

    @Value("${front.end.link}")
    public static String ALLOW_CORS_URL;
    @Value("${game.logic.url}")
    public static String GAME_LOGIC_URL;

}
