package be.kdg.catan.jwt.configuration;

import be.kdg.catan.shared.model.User;
import com.auth0.jwt.JWT;

import java.util.Date;

import static be.kdg.catan.jwt.configuration.SecurityConstants.EXPIRATION_TIME;
import static be.kdg.catan.jwt.configuration.SecurityConstants.SECRET;
import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

public class JWTGenerator {
    public static String generateToken(User user) {
        return JWT.create()
                .withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(HMAC512(SECRET.getBytes()));
    }
}
