package be.kdg.catan.jwt.configuration;

import be.kdg.catan.shared.model.User;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import static be.kdg.catan.jwt.configuration.SecurityConstants.SECRET;
import static be.kdg.catan.jwt.configuration.SecurityConstants.TOKEN_PREFIX;

public class JWTConvertor {
    public static String getUserNameFromToken(String token) {
        return JWT.require(Algorithm.HMAC512(SECRET.getBytes()))
                .build()
                .verify(token.replace(TOKEN_PREFIX, ""))
                .getSubject();
    }
}
