package be.kdg.catan.jwt.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestRestApi {
    @GetMapping("/test/user")
    public String testAccess() {
        return "woop";
    }
}
