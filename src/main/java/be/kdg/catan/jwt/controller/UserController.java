package be.kdg.catan.jwt.controller;

import be.kdg.catan.jwt.configuration.JWTGenerator;
import be.kdg.catan.jwt.exception.EmailAlreadyInUseException;
import be.kdg.catan.jwt.exception.EmailNotConfirmedException;
import be.kdg.catan.jwt.exception.UsernameAlreadyInUseException;
import be.kdg.catan.jwt.model.UserDTO;
import be.kdg.catan.shared.model.User;
import be.kdg.catan.shared.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;


/**
 * REST API to receive request depending on the user
 */
@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    private final ModelMapper modelMapper;
    @Value("${game.logic.url}")
    private String gameLogicUrl;
    private ObjectMapper objectMapper = new ObjectMapper();

    public UserController(UserService userService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    /**
     * API endpoint that recieves a user to register in the system
     *
     * @param user - the user to register in the system
     * @return - A response with the user and the CREATED code if user is register; otherwise the CONFLICT code is used.
     */
    // @CrossOrigin(origins = "${front.end.link}")
    @PostMapping("/sign-up")
    public ResponseEntity<?> signUp(@RequestBody User user) {
        System.out.println("USER: " + user);
        try {
            this.userService.registerUser(user);
            UserDTO userDTO = new UserDTO(user.getUsername(), user.getEmail());
            System.out.println("GAME LOGIC URL: " + gameLogicUrl);
            System.out.println(user.getUsername());
            System.out.println(userDTO.getUsername());
            ResponseEntity<?> response = new RestTemplate().postForEntity(gameLogicUrl + "/api/user", new HttpEntity<UserDTO>(userDTO), void.class);

        } catch (UsernameAlreadyInUseException | EmailAlreadyInUseException userAlreadyExists) {
            return new ResponseEntity<>(userAlreadyExists.getMessage(), HttpStatus.CONFLICT);
        }


        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }


    @GetMapping("/getByUsername/{username}")
    public ResponseEntity<User> getUser(@PathVariable String username) {
        List<User> users = userService.getUsers();
        User u = users.stream().filter(x -> x.getUsername().equals(username)).findFirst().get();
        return new ResponseEntity<>(userService.getUser(username), HttpStatus.OK);
    }

    @CrossOrigin(origins = "${game.logic.url}")
    @GetMapping("/users/getUsers")
    public ResponseEntity<List<UserDTO>> getUsers() {
        List<UserDTO> users = userService.getUsers().stream().map(x -> new UserDTO(x.getUsername(), x.getEmail())).collect(Collectors.toList());
        System.out.println(users.size());
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @CrossOrigin(origins = "${game.logic.url}")
    @GetMapping("/get/{username}")
    public ResponseEntity<List<UserDTO>> filterUsersByUsername(@PathVariable String username) {
        List<UserDTO> users = userService.findUsersByUsernameContaining(username).stream()
                .map(x -> new UserDTO(x.getUsername(), x.getEmail())).collect(Collectors.toList());
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping("/getByToken/{token}")
    public ResponseEntity<User> getUserByToken(@PathVariable("token") String token) {
        User user = userService.getUserByToken(token);

        return new ResponseEntity<>(user, HttpStatus.ACCEPTED);
    }

    /**
     * API endpoint that receives a user to log in
     *
     * @param user - the user to log into the system
     * @return - A response with the JWT token for the user and the ACCEPTED code; otherwise the BAD_REQUEST code is used.
     */
    @PostMapping("/sign-in")
    public ResponseEntity<?> signIn(@RequestBody User user) {
        try {
            userService.checkUser(user);
        } catch (BadCredentialsException | EmailNotConfirmedException bce) {
            return new ResponseEntity<>(bce.getMessage(), HttpStatus.BAD_REQUEST);
        }

        String token = JWTGenerator.generateToken(user);

        return new ResponseEntity<>(token, HttpStatus.ACCEPTED);
    }

    @PostMapping("/updateUser")
    public ResponseEntity<?> updateUser(@RequestBody User user) {

        userService.saveUser(user);
        return new ResponseEntity<>(user, HttpStatus.ACCEPTED);
    }
}
