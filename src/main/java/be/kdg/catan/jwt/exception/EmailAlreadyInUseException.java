package be.kdg.catan.jwt.exception;

/**
 * Exception thrown when a user tries to register the same email twice
 */
public class EmailAlreadyInUseException extends Throwable {
    public EmailAlreadyInUseException(String message) {
        super(message);
    }
}


