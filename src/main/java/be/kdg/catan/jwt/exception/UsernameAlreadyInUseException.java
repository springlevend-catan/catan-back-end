package be.kdg.catan.jwt.exception;

/**
 * Exception thrown when a user tries to register a username twice
 */
public class UsernameAlreadyInUseException extends Throwable {
    public UsernameAlreadyInUseException(String message) {
        super(message);
    }
}
