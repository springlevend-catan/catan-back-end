package be.kdg.catan.jwt.exception;

public class EmailNotConfirmedException extends Throwable {
    public EmailNotConfirmedException(String message) {
        super(message);
    }
}
