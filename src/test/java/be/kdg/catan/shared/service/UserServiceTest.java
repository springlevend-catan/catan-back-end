package be.kdg.catan.shared.service;

import be.kdg.catan.jwt.exception.EmailAlreadyInUseException;
import be.kdg.catan.jwt.exception.UsernameAlreadyInUseException;
import be.kdg.catan.shared.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class UserServiceTest {

    @Autowired
    private UserService userService;


    @Test(expected = UsernameAlreadyInUseException.class)
    public void registerUserExpectUsernameInUse() throws UsernameAlreadyInUseException, EmailAlreadyInUseException {
        User user1 = new User("TestUsername", "paswoord", "integratie@hotmail.be", "bart", "biemans", false,1);
        User user2 = new User("TestUsername", "paswoord", "integratie2@hotmail.be", "bart", "biemans", false,1);

        userService.registerUser(user1);
        userService.registerUser(user2);
    }

    @Test(expected = EmailAlreadyInUseException.class)
    public void registerUserExpectEmailInUse() throws UsernameAlreadyInUseException, EmailAlreadyInUseException {
        User user1 = new User("TestUsername", "paswoord", "robbeIntegratie@hotmail.be", "robbe", "peeters", false,1);
        User user2 = new User("TestUsername2", "paswoord", "robbeIntegratie@hotmail.be", "robbe", "peeters", false,1);

        userService.registerUser(user1);
        userService.registerUser(user2);
    }
}
